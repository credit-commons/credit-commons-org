# Credit Commons Org

Purpose, governance and constitution of the Credit Commons


## Intent
Develop a software stack that can serve a fully differentiated world economy of federated [Mutual Credit](https://en.wikipedia.org/wiki/Mutual_credit) networks, democratically governed by their members, with a 'local-first', bottom-up ethos.
The inspiration for this is the [Credit Commons whitepaper](http://www.creditcommons.net/) by [Matthew Slater](https://matslats.net/) and [Tim Jenkin](https://en.wikipedia.org/wiki/Tim_Jenkin).

## WHAT WE HAVE
1. [Functional specification for a self-federating Credit Commons protocol network](https://gitlab.com/credit-commons-software-stack/credit-commons-microservices/-/blob/master/docs/Accounting_TradeEngine_Fundamentals.md)
2. [Functional specification for a microservice implementation](https://gitlab.com/credit-commons-software-stack/credit-commons-microservices/-/blob/master/docs/Accounting_TradeEngine_microservice_architecture.md).
3. **Working [reference implementation of the microservices architecture](https://gitlab.com/credit-commons-software-stack/credit-commons-microservices). API-driven, written in PHP.** 
3. A core team - working under the name **CredCom** - working to build an enterprise-grade, open-source software stack, an investable business model and a viral 'scale-out' growth model. We are actively looking for funding.
4. A broad UK collaboration group - [Mutual Credit Services](http://mutualcredit.services) - exploring a wide range of niches where the Credit Commons can be seeded - in [Plymouth](http://sustenancepartners.com), in Brixton, [in the community sector](https://www.youtube.com/watch?v=3SSqPDSIXvI), among co-operatives...

## WHAT WE NEED
1. Co-design teams for
    - Credit Commons Economics
    - Credit Commons Governance
    - Credit Commons business model
    - Credit Commons investment model
    - Credit Commons legal framework

2. Contributors to
    - [Credit Commons software stack](https://gitlab.com/credit-commons-software-stack/) 
    - Credit Commons documentation and communications


## Stories

To illustrate what an economy of this kind could support, here are a few imaginary examples:

1. In Wales, UK, a group of ten rural co-operatives decides to set up a trading network. 
    - On the website of a regional inter-trade network, they use an online tool to select a domain name and form a minimal legal organisation with standard governance tools. Answering a few more questions{1}, their group is accepted for provisional membership, pending validation. This has cost them nothing more than time. {1: They have opted for the simplest start, accepting 'default' settings for most parameters, accepting the inter-trade network as 'operations' member in return for a fixed-term agreement to pay a tiny percentage fee on inter-network trades}.
    - Within an afternoon, each co-op is a member and they have made some trades among themselves. 
    - Within two weeks they are approved, have four more members and already one member has received a delivery of a spare part from a supplier in the local town, a member of the town business network, which is affiliated to the inter-trade network, and another has an enquiry about sheepskin slippers from Finland which has 'hopped' across five network tiers.
    - A week later, one of the member co-ops has used the same process to pop up a network (a b2e - business-to-employer - network) for its own members - making it possible for them to choose to take some of their wages in Mutual Credit tokens, and access any member of the whole network to spend them.
3. In a favela in Brazil, two younger members of the community propose setting up a local moneyless exchange network. People are sceptical - they have never heard of such a thing, and some don't have easy access to technology. 
     - The two make an arrangement with the local store, which has a computer and a printer, to act as the provider of one-time tokens. When a member wants to buy something, she can go to the coffee-shop - where she is well-known - and asks for some tokens. The shop owner asks her to authenticate her request on the computer, then prints the tokens for her, which can circulate until they arrive at any member who can redeem them back into the digital system (both printing and redeeming attract a small reward). 
    - The favela network connects with a city-wide network, allowing local value creation to pay for education for favela children, and favela people to be paid for work maintaining school grounds.
4. A world-wide network of film creatives takes the standard bundle of network-formation resources and forks it, adding a sophisticated mechanism that allows for incremental share development in one-off networks for film projects.
    - One aspect of the mechanism is the ability to sell distribution use-credits in return for fiat currency. The capacity to pay for labour from a central project fund in mutual credit and for external costs in fiat, all in accordance with a collaborative budget plan makes it possible to create films faster, with less need for rent-seeking investors. 
    - Later, the new tools are forked again and adapted by housing co-operatives for use in buying land and property into commons ownership.

## Technical requirements and raison d'être for this project
In order to arrive at such an economy, three technical provisions are considered crucial:

* straightforward ability for any group, anywhere in the world, with minimal technical or financial means, to set up the technical and practical tooling (interface, membership, directory, accounting, self-governance) a functionally viable Mutual Credit network that will have the capacity to federate with others.
* a sufficiently flexible set of interaction protocols to allow for a wide variety of differentiation between networks, without losing the capacity for federation with other networks for the purposes of inter-trade.
* for the technical implementations of these mechanisms to be licensed appropriately for community improvement, elaboration and differentiation.

In addition to these technical requirements there are, of course, some cultural requirements. People and businesses need to gain confidence and understanding around the capacities of complementary currencies, need to be able to see how they benefit other people who use them, need to believe that it is worth the additional overheads (of all kinds)  that are incurred for taking part.

These cultural developments are already under way [material is required here with links to many examples, statistics, research reports &c], but are severely hampered by the poor quality, lack of availability and inter-operability of the available technical tools.

This project considers that development such tools at this time can catalyse an explosion in complementary money system approaches.

The basis for each fundamental requirement of the project are linked . 

## Software
### Trading and Accounting engine
[Mutual Credit](https://en.wikipedia.org/wiki/Mutual_credit) trading and accounting are essentially normal, but with some specific differences. These are inherently simple, and can be (have been) implemented in a variety of ways.

As the benefits of Mutual Credit as a means of exchange currency are best developed inin a human-scale network of trust (think 150 / Dunbar number), open-ness of accounts, and a slightly more social interaction around trading than is typical are considered optimal.

The small size of such networks imposes strong limitations on the complexity of value-creation-chains that can be supported - hence the strong requirement for federation. Easy federation 'out of the box' imposes some additional requirements

A framework for the development of the Credit Commons in technical / software terms is under development in the [Credit Commons Microservices project](https://gitlab.com/credit-commons-software-stack/credit-commons-microservices). This is currently a private repo as  the software is at  'proof-of-concept' stage of development, and conversations about flavours of Open Source licensing take place. We welcome engagement - email dil @ dilgreen.net to request access.

This currently has:
   - [technology agnostic](https://gitlab.com/credit-commons/cc-php-lib/-/blob/master/docs/Accounting_TradeEngine_Fundamentals.md)
   - [microservice architecture specification](https://gitlab.com/credit-commons/cc-php-lib/-/blob/master/docs/Accounting_TradeEngine_microservice_architecture.md) 
   - [OpenAPI definition](https://gitlab.com/credit-commons/cc-php-lib/-/blob/master/docs/credit-commons-openapi-3.0.yml)

### Directory / messaging Service
Currencies are nothing without marketplaces within which they are accepted. Marketplaces are bounded contexts within which willing traders can discover each other and agreee terms of trade.

Since Credit Commons networks will for the foreseeable future comprise only a limited subset of the economy, mutual discoverability of members is crucial. Hence the need for Directories - typically these make provision for more specific 'offers' and 'wants' in addition to typical trade directory business descriptions. Price lists / catalogues are not typically provisioned.

Although simple to understand, the design decisions that must be made are non-trivial in terms of the character and usabibility of the directory. Typical approaches (use google at one extreme, search a network of members at the other) are not considered sufficient to effectively power the Credit Commons federation.

Integrated simple messaging is considered important.

The affiliated [Open Credit Network](http:/opencredit.network) has a simple but powerful directory tool in alpha development - see [Open Credit Network](trade.opencredit.network). This is currently a private repo while conversations about flavours of Open Source licensing take place - this should take place soon (by Sept '19)'.  We welcome engagement - email dil @ dilgreen.net to request access.

### Governance and Governance tools
The Credit Commons is predicated upon each network being under the democratic control of its members. At a minimum, four policy frameworks are required to regulate the function of the economy which results from network federation: regulation/authorisation of membership, setting of balance limits (positive and negative), setting of fees levied on transactions, setting of time limits for trade completion.

While it is proposed that the standard inception process of a new network is that a member of an existing network will use software to 'spawn  a minimal but complete child network which inherits the governance conditions of the parent network, a minimum viable governance system must form part of this to allow for democratic control of these policies.

Since Mutual Credit networks will typically involve binding members to a legal agreement (smart contract DAO style implementations are obviously imaginable but not currently on any roadmap), formation of such an organisation should be a simple part of the process of setting up a new network. 

On this basis, one approach (for the UK) is to fork the [one-click orgs](http://www.oneclickorgs.com/) project. Another possible approach is to use [Level](https://yourlevel.co/). A more 'root-and-branch' approach would build upon the analysis and propositions proposed under the heading [Sentient Commons](https://gitlab.com/the-sentient-commons/sentient-commons-outline/-/blob/master/README.md). 

### Groupware tools / Commons Culture
This aspect of the project is not currently well developed, and is the focus of the next phase of work.

Essentially, the tools used by the group (largely software mediated) should serve the development of trust, which is the real product of the 'Commons' that is being built.

The software must be designed around social needs of the Credit Commons networks.  This suggests that most groupware tools will be sub-optimal - either being framed in the context of orgs driven by simple success metrics/KPIs (with profit as the dominant underlying model), or as invisibly feudal social networks.

Consideration is being pais to the use / development of Pattern Language as a 'carrier wave' for this culture.

Practically, the use static, personal, immutable, decentralised record keeping on the basis of static site generation as per [Hugo](https://gohugo.io/) is under consideration.

## Legal

## Investability

## 'Seed governance'
We consider that this project is potentially precious - not precious in terms of money, but precious in terms of the value to humanity of a viable means-of-exchange system that operationalises and enshrines trust as the empowering mechanism of a global civilisation rooted in locality (valuing equally both physical and ontological locality).

Without doubt, this project, and the networks it aims to foster, must be Commons  institutions. A Commons is not a free-for-all: in a commons, there are commoners, with rights and responsibilities, and non-commoners, with fewer rights. A Commons has a boundary - a boundary that is drawn to ensure that what is valuable about the commons is nurtured.
The first and foremost responsibilities of the developers of the Credit Commons Stack is to ensure that the commons that we envisage building is, to the best of our ability, an unenclosable one.

What we have right now, though, is a seed, a sketch, nothing more. 

Our second duty is to nurture this seed; protect it, help it grow, develop, become strong. Strong enough to compete in a forest that is already full of aggressive and greedy giants.

If we are to do that, this small idea must become resilient, capable, robust, useful and strongly recognisable as a nameable thing - a Credit Commons. This builds a protection much stronger than any legal force - a strong cultural identity.

If we don't treat this seed as precious, but simply strew it far and wide in the hope that it will grow somewhere, we are likely to see all manner of straggly weeds and bizarre mutations, as projects are developed without our vision.

The worst outcome, of course, would be to see a tall, strong version of this thing which has been mutated to serve profit extraction, and which moves faster and with more certainty - because it can easily attract investment, because it has a simplistic measure of success - shareholder value - and nothing else.

Examples abound of just this process happening to wide-eyed optimistic projects - http/html, the idea of the sharing economy, Facebook's Libra. Open-source inventions thrown, with fanfare, to the wind, followed by extractive and self-serving 'embrace and extend'.

At present, we can't be more specific about how we will protect the project from enclosure. We do understand, of course, that to succeed, what we build must be freely accessible and open-source (our vision, clear as it is, may not be the optimal one - we must let others be free to try alternate approaches, to improve). 

Our inspiration is the Linux Foundation - which gives everything away except its brand, and which maintains its position sololey on the basis of self-evidently being the best place in the world for open-source *nix kernel work.

 We welcome engagement - email dil @ dilgreen.net to request access.
 
